#include "progress-bar.hpp"
#include <thread>

void standard_bar(const unsigned int niter,const unsigned int timeloop)
{
    {
   //demo of bar with scolling
    progressbar::cfg conf;
    conf.max_message_size=15;
    progressbar bs(niter,"I'm a very cool scrolling bar",conf);   
    for(std::cout<<bs;bs;std::cout<<++bs)
       std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<'\n';
   }
   
   {
    //demo of bar with unknow number of iteration (ex: loading file)
    std::string message="Iter-max unknown   ";
    progressbar b2(message);
    std::cout<<b2;
    for(unsigned int i=0;i<niter;++i,std::cout<<++b2)
        std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout << b2.end(niter)<<'\n';
   }
    {
    //demo of bar with nbitermax known and style TQDM
    progressbar::cfg conf;//{.style=progressbar::TQDM};
    conf.style=progressbar::TQDM;
    progressbar b(niter,"Iter-max known TQDM",conf);   
    for(std::cout<<b;b;std::cout<<++b)
       std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<'\n';
    }
    {
    //demo of bar with nbitermax known and style TENSORFLOW
    progressbar::cfg conf;//{.style=progressbar::TENSORFLOW};
    conf.style=progressbar::TENSORFLOW;
    progressbar b3(niter,"Iter-max known  TSF",conf);   
    for(std::cout<<b3;b3;std::cout<<++b3)  
        std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<std::endl;
    }
    
    {
    //demo of bar for resuming purpose (does not start to 0)
    progressbar::cfg conf;//{.style=progressbar::SHARP,.bar_size=40};
    conf.style=progressbar::SHARP;
    conf.bar_size=40;
    progressbar b4(niter/3,niter,"Resuming from to   ",conf);
    for(std::cout<<b4;b4;std::cout<<++b4)  
       std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<std::endl;
    }
}

void color_bar(const unsigned int niter,const unsigned int timeloop)
{
    progressbar::cfg conf;
    conf.style=progressbar::TQDM;
    conf.colorized=true;
    conf.barf_cfg={coit::fg::Blue,coit::bg::Default};
    conf.barc_cfg={coit::fg::Blue,coit::bg::Default};
    conf.bare_cfg={coit::fg::LightBlue,coit::bg::Default};
    conf.message_cfg={coit::fg::Blue,coit::bg::Default,coit::fmt::Underlined};
    {
        progressbar bc(niter,"I'am beautiful in color",conf);   
        for(std::cout<<bc;bc;std::cout<<++bc)  
            std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
        std::cout<<std::endl;
    }
    {   
        conf.style=progressbar::PIP;
        conf.bare_cfg={coit::fg::DarkGray,coit::bg::Default};
        progressbar bc(niter,"I'am beautiful in color",conf);   
        for(std::cout<<bc;bc;std::cout<<++bc)  
            std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
        std::cout<<std::endl;
    }
}

int main(int argc,char* argv[0])
{
    if(argc<3)
    {
        std::cerr<<argv[0]<<" "<<"<nbiter> <time/loop in ms>"<<std::endl;
        return 0;
    }
    const unsigned int niter=std::atoi(argv[1]);
    const unsigned int timeloop=std::atoi(argv[2]);
   
    standard_bar(niter,timeloop);
    
    color_bar(niter,timeloop);
    return 0;
}