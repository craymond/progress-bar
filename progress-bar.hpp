/* ============================================================================
    ___   ___   ___   __    ___   ____  __   __   ___    __    ___
   | |_) | |_) / / \ / /`_ | |_) | |_  ( (` ( (` | |_)  / /\  | |_)
   |_|   |_| \ \_\_/ \_\_/ |_| \ |_|__ _)_) _)_) |_|_) /_/--\ |_| \_
 
   ============================================================================
*/
/**
 * \file     progress-bar.hpp
 * \author   Christian Raymond
 * \version  1.0
 * \date     April, 7, 2021
 * \brief    progress bar in C++ + Timer class to measure time intervals
 *
 * \details         
 */

#ifndef PROGRESSBAR_HPP
#define PROGRESSBAR_HPP
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <chrono>
#include <string_view>
#include <iomanip>
#include <vector>
#include <cmath>
#include <array>
#include "coit.h"

using namespace std::literals::chrono_literals;


class Timer
{
	std::chrono::steady_clock::time_point _start=std::chrono::steady_clock::now();
	std::chrono::steady_clock::time_point _end;
public:
	Timer& end(){_end = std::chrono::steady_clock::now();return *this;};
	void start() {_start = std::chrono::steady_clock::now();}
    std::chrono::milliseconds time_from_start() const {return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-_start);}
    //nbiter to know time by iteration, 1 for total time
    std::string time_elapsed(const unsigned int nbiter=1,const bool precis=false) const
    {
        const auto s = std::max(nbiter, static_cast<unsigned int>(1));
        std::chrono::milliseconds total(std::chrono::duration_cast<std::chrono::milliseconds>(_end - _start));
        total /= s;
        std::ostringstream out;  
        display(out,total,precis);
        return out.str();
    }
    static std::ostream& display(std::ostream& out, std::chrono::milliseconds total,const bool precis=false)
    {
        std::chrono::hours   hh = std::chrono::duration_cast<std::chrono::hours>(total);
        total -= hh;
        std::chrono::minutes mm = std::chrono::duration_cast<std::chrono::minutes>(total);
        total -= mm;
        std::chrono::seconds ss = std::chrono::duration_cast<std::chrono::seconds>(total);
        total -= ss;
        std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(total);
        
        if(hh>std::chrono::hours(0))
            out << hh.count() << "h";
        if(mm>std::chrono::minutes(0))
            out << mm.count() << "m";
        if (ss>std::chrono::seconds(0))
            out << ss.count() << "s";
        if(precis)
            out << ms.count() << "ms";
        return out;
    } 
};

inline std::ostream& operator<<(std::ostream& out,std::chrono::milliseconds total)
{
    const std::chrono::minutes mm = std::chrono::duration_cast<std::chrono::minutes>(total);
    total -= mm;
    const std::chrono::seconds ss = std::chrono::duration_cast<std::chrono::seconds>(total);
    out << std::setw(2)<<std::setfill('0')<<std::fixed<< mm.count() << ":" <<std::setw(2)<<std::setfill('0')<< ss.count();
    return out;
}


class progressbar 
{
    using mint= unsigned int;
    using cmint = const unsigned int;
    using cdouble = const double;
    using cbool=const bool;
    using pchar = const char;
    using cschar = const char* const;
    
    public:
    enum STYLES {NOBAR,TENSORFLOW,SHARP,TQDM,PIP};
    struct cfg
    {
        STYLES style=TENSORFLOW;
        /*size of the progression bar*/
        mint bar_size=25;
        /*max character for the message*/
        mint max_message_size=30;
        /*refresh min*/
        std::chrono::milliseconds refresh=100ms; 
        /*bar colorized: default no*/
        bool colorized=false;
        /*color style of the message if colorized*/
        coit::text_cfg message_cfg={};
        /*color style of the fulled part of bar if colorized*/        
        coit::text_cfg barf_cfg={coit::fg::Red,coit::bg::Default};
        /*color style of the current part of bar if colorized*/
        coit::text_cfg barc_cfg={coit::fg::Red,coit::bg::Default};
        /*color style of the empty part of bar if colorized*/
        coit::text_cfg bare_cfg={coit::fg::DarkGray,coit::bg::Default};
        /*color style of elapsed time*/
        coit::text_cfg etime_cfg={coit::fg::Green,coit::bg::Default};
        /*color style of remaining time*/
        coit::text_cfg rtime_cfg={coit::fg::Red,coit::bg::Default};
        constexpr cfg(const STYLES s=TENSORFLOW,cmint bs=25, cmint bbs=30,const std::chrono::milliseconds r=100ms,cbool wc=false, const coit::text_cfg& mc={} ) : style(s), bar_size(bs), max_message_size(bbs),refresh(r), colorized(wc),message_cfg(mc) {}
    };
    private:
    struct Style
    {        
        cschar done_char;
        cschar current_char;
        cschar todo_char;
        cschar opening_bracket_char;
        cschar closing_bracket_char;
        constexpr Style(cschar d,cschar cu,cschar t,cschar o,cschar c) : done_char(d),current_char(cu),todo_char(t),opening_bracket_char(o),closing_bracket_char(c) {}
        constexpr static Style get(const STYLES s)
        {
            //constexpr Style LIST_OF_STYLES[] = { {"","","","",""}, {"=",">",".","[","]"}, {"#",".", ".","[","]"}, {"█","▒", "░","|","|"}};              
            constexpr std::array<Style,5> LIST_OF_STYLES = { Style{"","","","",""}, Style{"=",">",".","[","]"}, Style{"#",".", ".","[","]"}, Style{"█","▒", "░","|","|"}, Style{"─","►", "─"," "," "}};           
            //assert(s<LIST_OF_STYLES);
            return LIST_OF_STYLES[s];
        }
    };
       
        const std::string _message;
        double _iters_done=0; //total of iteration done 
        cdouble _iters_done_previous=0; //total of iteration done in a previous progressbar
        mint _total_iters; 
        const cfg& _cfg;
        cbool _do_show_bar;
        const Style _style;              
        Timer t;
        
        mutable std::chrono::steady_clock::time_point _last_call; //remember last percentage to avoid too frequent refresh 
        mutable std::size_t _start_scrolling=0; //for scrolling long message > max_message_size
        
        constexpr static coit::text_cfg _default_cfg{};

        void update(cdouble add=1){ _iters_done+=add;}
        progressbar           (const progressbar&)     = default;
        progressbar           (progressbar&&) noexcept = default;
        std::string message() const
        {
            if(_message.size()<=_cfg.max_message_size) return _message;

            const std::string& pad=_cfg.max_message_size >_message.size()-_start_scrolling ? std::string(" ")+_message.substr(0,_cfg.max_message_size-(_message.size()-_start_scrolling)-1) : "";
            const std::string& mess=_message.substr(_start_scrolling,_cfg.max_message_size)+pad;
            
            ++_start_scrolling;
            if (_start_scrolling>= _message.size()) _start_scrolling=0;
            return mess;
        }       
        std::string manip(const coit::text_cfg& c) const
        {
            if(this->_cfg.colorized)
            {
                std::ostringstream m;
                m << c;
                return m.str();
            }
            else
                return "";
        }
        std::ostream& printbar(std::ostream& out) const
        {
            auto now=std::chrono::steady_clock::now();
            if(_iters_done<_total_iters && std::chrono::duration_cast<std::chrono::milliseconds>(now-_last_call)< _cfg.refresh) return out;
            _last_call=now;

            if(_iters_done>_total_iters) return out; //in case of too much call to update "user bad use"
            cmint perc_done=std::round(_iters_done*100/_total_iters);
            //if (last_perc == perc_done) return out;
            const std::string ti=std::to_string(_total_iters);
            const auto time=t.time_from_start();            

            out << '\r' << manip(_cfg.message_cfg) << message() << manip(_default_cfg);//_message; //message            
            
            if(_total_iters<std::numeric_limits<mint>::max()) 
                out << " ["<<std::setw(3)<<std::setfill(' ')<< std::fixed << perc_done <<"%] "; //percentage                     
            if(_do_show_bar)
            {   
                const int nbd=perc_done*_cfg.bar_size/100;
                const int nbt=_cfg.bar_size-nbd-1;                
                out << _style.opening_bracket_char ;
                for(int i=0;i<nbd;++i) out<<  manip(_cfg.barf_cfg) <<_style.done_char;
                if(perc_done<100) out<<manip(_cfg.barc_cfg)<<_style.current_char;
                for(int i=0;i<nbt;++i) out<<manip(_cfg.bare_cfg)<<_style.todo_char;                
                out << manip(_default_cfg)<< _style.closing_bracket_char<< " ";                
            }
            
            cdouble iters_in_this_bar=_iters_done-_iters_done_previous;           
            if(_total_iters<std::numeric_limits<mint>::max()) //we know the total nmber of iter to do
            {
                out << std::setw(ti.size())<<std::setfill(' ')<< std::fixed << static_cast<cmint>(std::round(_iters_done))<<"/"<<ti; //iterations
                //estimation remaining time
                cmint perc_done_in_this_progress=std::round(iters_in_this_bar*100/(_total_iters-_iters_done_previous));
                cmint ratio=(100.0-perc_done_in_this_progress)/perc_done_in_this_progress*100; //multiplicateur 100 pour que le temps restant soit moins discret
                
                out<< " [ "<< manip(_cfg.etime_cfg) << time<< manip(_default_cfg) << "<"<< manip(_cfg.rtime_cfg) << time*ratio/100 << manip(_default_cfg);  //time         
            }
            else //we does not know the total number of iter
            {
                out << std::setw(ti.size())<<std::setfill(' ')<< std::fixed << _iters_done; //iterations
                out<< " [ "<< time<<"<??:?? ";         
            }
            out <<" ";
            const auto millisecond_from_start=t.time_from_start().count();
            if (millisecond_from_start>0) 
                out << std::setprecision(2)<< iters_in_this_bar*1000.0/millisecond_from_start;
            else 
                out << "?";
            out <<"it/s ] "; 
            out << std::flush;
            return out;
        }
    public:
        
        progressbar& operator=(const progressbar&) = delete;      
        progressbar& operator=(progressbar&&)      = delete;


        /**
         * \brief   progress-bar constructor
         * \param   total_iter number of total iteration
         * \param   message message to display before the bar
         * \param   cfg configuration of progress bar
         */
        progressbar(cmint total_iter,const std::string& message="", const cfg& pcfg=cfg()):_message(message), _total_iters(total_iter), _cfg(pcfg), _do_show_bar(pcfg.style!=NOBAR), _style(Style::get(pcfg.style)) { }
        progressbar(cdouble iter_previously_done,cmint total_iter,const std::string& message="", const cfg& pcfg=cfg()):  _message(message), _iters_done(iter_previously_done), _iters_done_previous(iter_previously_done), _total_iters(total_iter), _cfg(pcfg), _do_show_bar(pcfg.style!=NOBAR), _style(Style::get(pcfg.style))  { }
        progressbar(const std::string& message="", const cfg& pcfg=cfg()): _message(message), _total_iters(std::numeric_limits<mint>::max()), _cfg(pcfg), _do_show_bar(false), _style(Style::get(NOBAR)) { }
        // operators       
        explicit operator bool() const { return _iters_done!=_total_iters;}
        progressbar& operator=(cdouble i) {_iters_done=i;return *this;}
        progressbar& operator+=(cdouble i) {this->update(i);return *this;}
        progressbar operator++(int){ progressbar before(*this); this->update(1);return before;}
        progressbar& operator++(){this->update(1);return *this;}
        progressbar& end(cdouble f) {_iters_done=_total_iters=f; return *this;}        
        friend std::ostream& operator<<(std::ostream& out,const progressbar& p)
        {
            return p.printbar(out);
        }
};

#endif
