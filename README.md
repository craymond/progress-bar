# progress-bar: a progression bar for C++

- print iteration number
- print percentage completion
- display progression bar
- customizable bar style/size
- display elapsed time
- estimate remaining time
- may be in color

___

## Constructor

```cpp
progressbar::progressbar(
        cmint total_iter,
        const std::string& message = "",
        const progressbar::cfg pcfg = cfg())
    )
```

Parameters:

- **total_iter**: number of total iteration
- **message** : message to display before the bar
- **pcfg** : customization of the bar using the struct *progressbar::cfg*

## Customization

### bar customization

```cpp
progressbar::cfg
    {
        STYLES style=TENSORFLOW;
        mint bar_size=25;
        mint max_message_size=30;
        std::chrono::milliseconds refresh=100ms; 
        bool colorized=false;
        coit::text_cfg message_cfg={coit::fg::Default,coit::bg::Default,coit::fmt::Default};        
        coit::text_cfg barf_cfg={coit::fg::Red,coit::bg::Default};
        coit::text_cfg barc_cfg={coit::fg::Red,coit::bg::Default};
        coit::text_cfg bare_cfg={coit::fg::DarkGray,coit::bg::Default};       
        coit::text_cfg etime_cfg={coit::fg::Green,coit::bg::Default};
        coit::text_cfg rtime_cfg={coit::fg::Red,coit::bg::Default};
    };
```

- **style**: bar style among `[TENSORFLOW,SHARP,TQDM]`
- **bar_size**: bar size, longer is more precise
- **max_message_size**: box size for message: message will scroll if the message is bigger than box
- **refresh**: in millisecond, make sure to not refresh during this time, default 100ms
- **colorized**: bar in color or not
- **message_cfg**: color format of the message
- **barf_cfg**: color format of the fill part of the bar
- **barc_cfg**: color format of the symbol indication the current position
- **bare_cfg**: color format of the empty part of the bar itself
- **etime_fcfg**: color format of the elpased time
- **rtime_fcfg**: color format of the remaining time

### color customization

using a **triplet** {foreground color,background color,text format}, color are defined ![here](https://gitlab.inria.fr/craymond/coit).

exemple for a text in bold red on black `{coit::fg::Red,coit::bg::Black,coit::fmt::Bold}`
___

## C++ code example

```cpp
#include "progress-bar.hpp"
#include <thread>

void standard_bar(const unsigned int niter,const unsigned int timeloop)
{
    {
   //demo of bar with scolling
    progressbar::cfg conf;
    conf.max_message_size=15;
    progressbar bs(niter,"I'm a very cool scrolling bar",conf);   
    for(std::cout<<bs;bs;std::cout<<++bs)
       std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<'\n';
   }
   
   {
    //demo of bar with unknow number of iteration (ex: loading file)
    std::string message="Iter-max unknown   ";
    progressbar b2(message);
    std::cout<<b2;
    for(uint i=0;i<niter;++i,std::cout<<++b2)
        std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout << b2.end(niter)<<'\n';
   }
    {
    //demo of bar with nbitermax known and style TQDM
    progressbar::cfg conf;//{.style=progressbar::TQDM};
    conf.style=progressbar::TQDM;
    progressbar b(niter,"Iter-max known TQDM",conf);   
    for(std::cout<<b;b;std::cout<<++b)
       std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<'\n';
    }
    {
    //demo of bar with nbitermax known and style TENSORFLOW
    progressbar::cfg conf;//{.style=progressbar::TENSORFLOW};
    conf.style=progressbar::TENSORFLOW;
    progressbar b3(niter,"Iter-max known  TSF",conf);   
    for(std::cout<<b3;b3;std::cout<<++b3)  
        std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<std::endl;
    }
    
    {
    //demo of bar for resuming purpose (does not start to 0)
    progressbar::cfg conf;//{.style=progressbar::SHARP,.bar_size=40};
    conf.style=progressbar::SHARP;
    conf.bar_size=40;
    progressbar b4(niter/3,niter,"Resuming from to   ",conf);
    for(std::cout<<b4;b4;std::cout<<++b4)  
       std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
    std::cout<<std::endl;
    }
}

void color_bar(const unsigned int niter,const unsigned int timeloop)
{
    progressbar::cfg conf;
    conf.style=progressbar::TQDM;
    conf.colorized=true;
    conf.barf_cfg={coit::fg::Blue,coit::bg::Default};
    conf.barc_cfg={coit::fg::Blue,coit::bg::Default};
    conf.bare_cfg={coit::fg::LightBlue,coit::bg::Default};
    conf.message_cfg={coit::fg::Blue,coit::bg::Default,coit::fmt::Underlined};
    {
        progressbar bc(niter,"I'am beautiful in color",conf);   
        for(std::cout<<bc;bc;std::cout<<++bc)  
            std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
        std::cout<<std::endl;
    }
    {   
        conf.style=progressbar::PIP;
        conf.bare_cfg={coit::fg::DarkGray,coit::bg::Default};
        progressbar bc(niter,"I'am beautiful in color",conf);   
        for(std::cout<<bc;bc;std::cout<<++bc)  
            std::this_thread::sleep_for( std::chrono::milliseconds(timeloop) );
        std::cout<<std::endl;
    }
}

int main(int argc,char* argv[0])
{
    if(argc<3)
    {
        std::cerr<<argv[0]<<" "<<"<nbiter> <time/loop in ms>"<<std::endl;
        return 0;
    }
    const unsigned int niter=std::atoi(argv[1]);
    const unsigned int timeloop=std::atoi(argv[2]);
   
    standard_bar(niter,timeloop);
    
    color_bar(niter,timeloop);
    return 0;
}
```

## Output

![Alt Text](bar.gif)
